﻿using UnityEngine;
using static _2048Accelerom3D.ConstantsAndMore;

#if UNITY_EDITOR
using Input = _2048Accelerom3D.Testing.Input;
#endif

namespace _2048Accelerom3D
{
    public class AccelerometerControl : GameplayControl
    {
        private MoveDirection lastDirection;
        private bool maxValueReached;
        private bool cameraRotatedBackOnGameOver;

        public override void Update()
        {
            if (!CameraGoingBack && !GameController.Instance.GameOver)
            {
                Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation,
                                                                          defaultCameraRotation *
                                                                          Quaternion.AngleAxis(Input.acceleration.x * ACCELEROMETER_CONTROL_CAMERA_ROTATE_MULTIPLIER, Vector3.up) *
                                                                          Quaternion.AngleAxis(Input.acceleration.y * ACCELEROMETER_CONTROL_CAMERA_ROTATE_MULTIPLIER, -Vector3.right),
                                                                          Time.deltaTime * ACCELEROMETER_CONTROL_CAMERA_ROTATE_SPEED);

                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, defaultCameraPosition - new Vector3(Input.acceleration.x * ACCELEROMETER_CONTROL_CAMERA_MOVE_MULTIPLIER, 0F, Input.acceleration.y * ACCELEROMETER_CONTROL_CAMERA_MOVE_MULTIPLIER), Time.deltaTime * ACCELEROMETER_CONTROL_CAMERA_MOVE_SPEED);
            }
            else if (GameController.Instance.GameOver && !cameraRotatedBackOnGameOver)
            {
                cameraRotatedBackOnGameOver = true;
                RotateCameraBack();
            }

            if (CameraGoingBack || NumberController.MovingNumbersCount > 0 || GameController.Instance.GameOver)
                return;

            TryToMoveNumbers();
        }

        private void TryToMoveNumbers()
        {
            MoveDirection currentDirection;

            bool canMoveToTheSameDirection = !maxValueReached;

            if (Mathf.Abs(Input.acceleration.x) > Mathf.Abs(Input.acceleration.y))
            {
                if (maxValueReached = Input.acceleration.x > ACCELEROMETER_CONTROL_MAX_VALUE)
                    currentDirection = MoveDirection.Right;
                else if (maxValueReached = -Input.acceleration.x > ACCELEROMETER_CONTROL_MAX_VALUE)
                    currentDirection = MoveDirection.Left;
                else
                    return;
            }
            else
            {
                if (maxValueReached = Input.acceleration.y > ACCELEROMETER_CONTROL_MAX_VALUE)
                    currentDirection = MoveDirection.Up;
                else if (maxValueReached = -Input.acceleration.y > ACCELEROMETER_CONTROL_MAX_VALUE)
                    currentDirection = MoveDirection.Down;
                else
                    return;
            }

            if (lastDirection != currentDirection || canMoveToTheSameDirection)
                lastDirection = GameController.Instance.MoveNumbers(currentDirection);
        }
    }
}