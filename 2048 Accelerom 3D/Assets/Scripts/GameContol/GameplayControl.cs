﻿using System.Collections;
using UnityEngine;

namespace _2048Accelerom3D
{
    public abstract class GameplayControl
    {
        protected Quaternion defaultCameraRotation;
        protected Vector3 defaultCameraPosition;
        protected MoveAxis moveAxis;

        protected bool CameraGoingBack { get; set; }

        public abstract void Update();

        public void RotateCameraBack(System.Action callback = null)
        {
            GameController.Instance.StartCoroutine(InnerRotateCameraBack());
            IEnumerator InnerRotateCameraBack()
            {
                CameraGoingBack = true;
                while (Camera.main.transform.rotation != defaultCameraRotation || Camera.main.transform.position != defaultCameraPosition)
                {
                    Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, defaultCameraRotation, Time.deltaTime * ConstantsAndMore.CAMERA_ROTATE_BACK_SPEED);
                    Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, defaultCameraPosition, Time.deltaTime * ConstantsAndMore.CAMERA_MOVE_BACK_SPEED);
                    yield return null;
                }
                CameraGoingBack = false;

                callback?.Invoke();
            }
        }

        public virtual void Cancel(System.Action callback) => RotateCameraBack(callback);

        protected GameplayControl()
        {
            defaultCameraRotation = Camera.main.transform.rotation;
            defaultCameraPosition = Camera.main.transform.position;
        }
    }
}
