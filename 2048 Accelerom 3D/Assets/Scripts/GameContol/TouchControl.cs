﻿using UnityEngine;
using static _2048Accelerom3D.ConstantsAndMore;

namespace _2048Accelerom3D
{
    public class TouchControl : GameplayControl
    {
        private bool touchDown;
        private Vector2 touchDownPosition;
        private int touchDownIndex = -1;

        private void TouchUp()
        {
            touchDown = false;
            moveAxis = MoveAxis.None;
            touchDownPosition = Vector3.zero;
            touchDownIndex = -1;
            RotateCameraBack();
        }

        private void TouchDown(Vector2 position, int index)
        {
            touchDown = true;
            touchDownPosition = position;
            touchDownIndex = index;
        }

        private void InnerUpdate()
        {
            float touchDifferenceAbsX = Mathf.Abs(Input.GetTouch(touchDownIndex).position.x - touchDownPosition.x);
            float touchDifferenceAbsY = Mathf.Abs(Input.GetTouch(touchDownIndex).position.y - touchDownPosition.y);

            if (moveAxis == MoveAxis.None && !CameraGoingBack && !GameController.Instance.GameOver)
            {
                if (touchDifferenceAbsX > touchDifferenceAbsY && touchDifferenceAbsX > TOUCH_CONTROL_MIN_VALUE * Screen.dpi)
                    moveAxis = MoveAxis.Horizontal;
                else if (touchDifferenceAbsY > touchDifferenceAbsX && touchDifferenceAbsY > TOUCH_CONTROL_MIN_VALUE * Screen.dpi)
                    moveAxis = MoveAxis.Vertical;
            }

            if (moveAxis != MoveAxis.None)
            {
                Vector2 touchDifference = Input.GetTouch(touchDownIndex).position - touchDownPosition;

                if (moveAxis == MoveAxis.Horizontal && touchDifferenceAbsX > TOUCH_CONTROL_MAX_VALUE * Screen.dpi)
                {
                    RotateCameraBack();
                    GameController.Instance.MoveNumbers(touchDifference.x > 0 ? MoveDirection.Right : MoveDirection.Left);
                    TouchUp();
                }
                else if (moveAxis == MoveAxis.Vertical && touchDifferenceAbsY > TOUCH_CONTROL_MAX_VALUE * Screen.dpi)
                {
                    RotateCameraBack();
                    GameController.Instance.MoveNumbers(touchDifference.y > 0 ? MoveDirection.Up : MoveDirection.Down);
                    TouchUp();
                }
                else
                {
                    if (moveAxis == MoveAxis.Horizontal)
                    {
                        Camera.main.transform.rotation = defaultCameraRotation * Quaternion.AngleAxis(touchDifference.x * TOUCH_CONTROL_CAMERA_ROTATE_SPEED / Screen.dpi, Vector3.up);
                        Camera.main.transform.position = defaultCameraPosition - new Vector3(touchDifference.x * TOUCH_CONTROL_CAMERA_MOVE_SPEED / Screen.dpi, 0F, 0F);
                    }
                    else
                    {
                        Camera.main.transform.rotation = defaultCameraRotation * Quaternion.AngleAxis(touchDifference.y * TOUCH_CONTROL_CAMERA_ROTATE_SPEED / Screen.dpi, -Vector3.right);
                        Camera.main.transform.position = defaultCameraPosition - new Vector3(0F, 0F, touchDifference.y * TOUCH_CONTROL_CAMERA_MOVE_SPEED / Screen.dpi);
                    }
                }
            }
        }

        public override void Update()
        {
            if (!touchDown && !CameraGoingBack && NumberController.MovingNumbersCount == 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Touch touch = Input.GetTouch(i);
                    if (touch.phase == TouchPhase.Began &&
                        touch.position.x >= TOUCH_CONTROL_SCREEN_LEFT_BORDER && touch.position.x <= TOUCH_CONTROL_SCREEN_RIGHT_BORDER &&
                        touch.position.y >= TOUCH_CONTROL_SCREEN_DOWN_BORDER && touch.position.y <= TOUCH_CONTROL_SCREEN_UP_BORDER)
                    {
                        TouchDown(touch.position, i);
                        break;
                    }
                }
            }
            else if (touchDown && Input.GetTouch(touchDownIndex).phase == TouchPhase.Ended)
            {
                TouchUp();
            }

            if (touchDown)
            {
                InnerUpdate();
            }
        }

        public override void Cancel(System.Action callback)
        {
            TouchUp();
            base.Cancel(callback);
        }
    }
}
