using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using static _2048Accelerom3D.ConstantsAndMore;

using Random = UnityEngine.Random;

namespace _2048Accelerom3D
{
    class GameController : MonoBehaviour
    {
        internal static GameController Instance { get; private set; }

        [SerializeField]
        private Material fireWorldSkyboxMaterial;
        public Material FireWorldSkyboxMaterial => fireWorldSkyboxMaterial;

        [SerializeField]
        private ParticleSystem fireWorldGameOverFirePrefab;
        public ParticleSystem FireWorldGameOverFirePrefab => fireWorldGameOverFirePrefab;

        [SerializeField]
        private ParticleSystem fireWorldGameOverExplosionPrefab;
        public ParticleSystem FireWorldGameOverExplosionPrefab => fireWorldGameOverExplosionPrefab;

        [SerializeField]
        private Transform waterWorldAdditionsPrefab;
        public Transform WaterWorldAdditionsPrefab => waterWorldAdditionsPrefab;

        [SerializeField]
        private NumberController fireNumberPrefab;
        public NumberController FireNumberPrefab => fireNumberPrefab;

        [SerializeField]
        private NumberController waterNumberPrefab;
        public NumberController WaterNumberPrefab => waterNumberPrefab;

        [SerializeField]
        private Transform fireGroundPrefab;
        public Transform FireGroundPrefab => fireGroundPrefab;


        [SerializeField]
        private Transform waterGroundPrefab;
        public Transform WaterGroundPrefab => waterGroundPrefab;


        [SerializeField]
        private Transform fireWallPrefab;
        public Transform FireWallPrefab => fireWallPrefab;


        [SerializeField]
        private Transform waterWallPrefab;
        public Transform WaterWallPrefab => waterWallPrefab;


        [SerializeField]
        private float wallTextureSize = 1F;

        [SerializeField]
        private int mapSize;

        [SerializeField]
        private float numbersDelta = .25F;

        [SerializeField]
        private float cameraMoveSpeed;

        [SerializeField]
        private Transform gameOverInfo;

        private Transform mapTransform;

        private Vector3[,] numbersCoordinates;

        public NumberController[,] NumbersControllers { get; private set; }

        private Transform groundTransform;

        private Transform[] wallsTransform;

        private GameplayControl gameplayControl;

        public IWorldType WorldTypeDependency { get; private set; }

        public bool GameOver { get; private set; }

        public bool CanTapToPlayAgain { get; set; }

        private int destructingCount;

        private void Awake() => Instance = this;

        private void Start()
        {
            WorldTypeDependency = Settings.FireWorldSelected ? (IWorldType)new FireWorld() : new WaterWorld();
            WorldTypeDependency.OnStart();

            mapTransform = new GameObject("Map").transform;

            float groundScale = CreateGround();
            CreateWalls(groundScale);

            CalculateNumbersCoordinates();
            for (int i = 0; i < START_TIME_NUMBERS_COUNT; i++)
            {
                CreateNewNumberAndCheckGameOver();
            }

            SetCamera(groundScale);

            gameplayControl = Settings.UseAccelerometer ? (GameplayControl)new AccelerometerControl() : new TouchControl();
        }

        private void SetCamera(float groundScale)
        {
            Camera.main.transform.rotation = Quaternion.Euler(90F, 0F, 0F);
            Camera.main.transform.position = new Vector3((groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.x) / 2F, 5F, (groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.z) / 2F);
        }

        private void CalculateNumbersCoordinates()
        {
            numbersCoordinates = new Vector3[mapSize, mapSize];
            Vector3 currentCoordinates = Vector3.zero;
            for (int row = 0; row < mapSize; row++)
            {
                currentCoordinates.x = 0;
                for (int column = 0; column < mapSize; column++)
                {
                    numbersCoordinates[row, column] = currentCoordinates;
                    currentCoordinates.x += WorldTypeDependency.NumberPrefab.transform.localScale.x + numbersDelta;
                }
                currentCoordinates.z += WorldTypeDependency.NumberPrefab.transform.localScale.z + numbersDelta;
            }
        }

        private float CreateGround()
        {
            groundTransform = Instantiate(WorldTypeDependency.GroundPrefab, mapTransform);
            float groundScale = mapSize * WorldTypeDependency.NumberPrefab.transform.localScale.x + (mapSize - 1) * numbersDelta;
            groundTransform.localScale = new Vector3(groundScale / 10F, 1F, groundScale / 10F);
            groundTransform.position = new Vector3(groundScale / 2F - WorldTypeDependency.NumberPrefab.transform.localScale.x / 2F, -WorldTypeDependency.NumberPrefab.transform.localScale.y / 2F, groundScale / 2F - WorldTypeDependency.NumberPrefab.transform.localScale.z / 2F);

            return groundScale;
        }

        private void CreateWalls(float groundScale)
        {
            wallsTransform = new Transform[4];
            Vector3 horizontalWallScale = new Vector3(groundScale, WorldTypeDependency.WallPrefab.localScale.y, WorldTypeDependency.WallPrefab.localScale.z);
            Vector3 verticalWallScale = new Vector3(groundScale + WorldTypeDependency.WallPrefab.localScale.z * 2F, WorldTypeDependency.WallPrefab.localScale.y, WorldTypeDependency.WallPrefab.localScale.z);

            float wallYPosition = -(WorldTypeDependency.NumberPrefab.transform.localScale.y / 2F - WorldTypeDependency.WallPrefab.localScale.y / 2F);
            for (int i = 0; i < wallsTransform.Length; i++)
            {
                Transform current = Instantiate(WorldTypeDependency.WallPrefab, mapTransform);
                Material currentRendererMaterial = current.GetComponent<Renderer>().material;

                switch (i)
                {
                    case 0: // Up
                        current.localScale = horizontalWallScale;
                        currentRendererMaterial.mainTextureScale = horizontalWallScale / wallTextureSize;
                        currentRendererMaterial.SetTextureScale("_BumpMap", horizontalWallScale / wallTextureSize);
                        current.rotation = Quaternion.identity;
                        current.position = new Vector3((groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.x) / 2F, wallYPosition, groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.z / 2F + WorldTypeDependency.WallPrefab.localScale.z / 2F);
                        break;
                    case 1: // Right
                        current.localScale = verticalWallScale;
                        currentRendererMaterial.mainTextureScale = verticalWallScale / wallTextureSize;
                        currentRendererMaterial.SetTextureScale("_BumpMap", verticalWallScale / wallTextureSize);
                        current.rotation = Quaternion.Euler(0F, 90F, 0F);
                        current.position = new Vector3(groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.x / 2F + WorldTypeDependency.WallPrefab.localScale.x / 2F, wallYPosition, (groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.z) / 2F);
                        break;
                    case 2: // Down
                        current.localScale = horizontalWallScale;
                        currentRendererMaterial.mainTextureScale = horizontalWallScale / wallTextureSize;
                        currentRendererMaterial.SetTextureScale("_BumpMap", horizontalWallScale / wallTextureSize);
                        current.rotation = Quaternion.Euler(0F, 180F, 0F);
                        current.position = new Vector3((groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.x) / 2F, wallYPosition, (WorldTypeDependency.NumberPrefab.transform.localScale.z + WorldTypeDependency.WallPrefab.localScale.z) / -2F);
                        break;
                    case 3: // Left
                        current.localScale = verticalWallScale;
                        currentRendererMaterial.mainTextureScale = verticalWallScale / wallTextureSize;
                        currentRendererMaterial.SetTextureScale("_BumpMap", verticalWallScale / wallTextureSize);
                        current.rotation = Quaternion.Euler(0F, -90F, 0F);
                        current.position = new Vector3((WorldTypeDependency.NumberPrefab.transform.localScale.x + WorldTypeDependency.WallPrefab.localScale.x) / -2F, wallYPosition, (groundScale - WorldTypeDependency.NumberPrefab.transform.localScale.z) / 2F);
                        break;
                }
                wallsTransform[i] = current;
            }
        }

        private void CreateNewNumberAndCheckGameOver()
        {
            if (NumbersControllers == null)
                NumbersControllers = new NumberController[mapSize, mapSize];

            StartCoroutine(WaitForMoving());
            IEnumerator WaitForMoving()
            {
                while (NumberController.MovingNumbersCount > 0 || destructingCount > 0)
                {
                    yield return null;
                }

                if ((from NumberController number in NumbersControllers
                     where !number
                     select number).Count() != 0)
                {
                    int row, column;
                    do
                    {
                        row = Random.Range(0, mapSize);
                        column = Random.Range(0, mapSize);
                    } while (NumbersControllers[row, column]);

                    NumbersControllers[row, column] = Instantiate(WorldTypeDependency.NumberPrefab, numbersCoordinates[row, column], Quaternion.identity, mapTransform);
                    NumbersControllers[row, column].Number = Random.Range(0, NEW_NUMBER_IS_TWO_POSSIBILITY) == 0 ? 4 : 2;
                }

                if (GameOver = IsGameOver())
                    WorldTypeDependency.OnGameOver(() => gameOverInfo.gameObject.SetActive(true));
            }
        }

        public MoveDirection MoveNumbers(MoveDirection direction)
        {
            if (NumberController.MovingNumbersCount > 0)
                return MoveDirection.None;

            bool somthingMoved = false;

            switch (direction)
            {
                case MoveDirection.Up:
                case MoveDirection.Down:

                    for (int column = 0; column < mapSize; column++)
                    {
                        int step = 0;

                        for (int row = direction == MoveDirection.Up ? mapSize - 1 : 0;
                                       direction == MoveDirection.Up ? row >= 0 : row < mapSize;
                                         row += direction == MoveDirection.Up ? -1 : 1)
                        {
                            InLoop(row, column, ref step);
                        }
                    }

                    break;
                case MoveDirection.Right:
                case MoveDirection.Left:

                    for (int row = 0; row < mapSize; row++)
                    {
                        int step = 0;
                        for (int column = direction == MoveDirection.Right ? mapSize - 1 : 0;
                            direction == MoveDirection.Right ? column >= 0 : column < mapSize;
                            column += direction == MoveDirection.Right ? -1 : 1)
                        {
                            InLoop(row, column, ref step);
                        }
                    }
                    break;
                default:
                    throw new ArgumentException("Unknown movement direction.");
            }

            if (somthingMoved)
                CreateNewNumberAndCheckGameOver();

            return direction;

            void InLoop(int row, int column, ref int step)
            {
                NumberController currentNumber = NumbersControllers[row, column];

                if (currentNumber)
                {
                    int startTimeStep = step;
                    if (CheckTheSameNumber(row, column, out NumberController theSameNumber) && !theSameNumber.CannotUse)
                        step++;

                    (int Row, int Column) newPosition = (row, column);
                    switch (direction)
                    {
                        case MoveDirection.Up:
                            newPosition.Row += step;
                            break;
                        case MoveDirection.Right:
                            newPosition.Column += step;
                            break;
                        case MoveDirection.Down:
                            newPosition.Row -= step;
                            break;
                        case MoveDirection.Left:
                            newPosition.Column -= step;
                            break;
                    }

                    if (row != newPosition.Row || column != newPosition.Column)
                    {
                        somthingMoved = true;

                        if (theSameNumber && !theSameNumber.CannotUse)
                        {
                            currentNumber.CannotUse = true;
                            destructingCount++;
                            theSameNumber.WaitForDestruction(currentNumber, numbersCoordinates[newPosition.Row, newPosition.Column], () =>
                            {
                                currentNumber.Number *= 2;
                                destructingCount--;
                            });
                        }

                        NumbersControllers[newPosition.Row, newPosition.Column] = NumbersControllers[row, column];
                        NumbersControllers[row, column] = null;
                        NumbersControllers[newPosition.Row, newPosition.Column].Move(numbersCoordinates[newPosition.Row, newPosition.Column]);
                    }
                }
                else
                    step++;
            }

            bool CheckTheSameNumber(int originalRow, int originalColumn, out NumberController foundNumber)
            {
                foundNumber = null;
                switch (direction)
                {
                    case MoveDirection.Up:
                    case MoveDirection.Down:
                        for (int row = originalRow + (direction == MoveDirection.Up ? 1 : -1);
                             direction == MoveDirection.Up ? row < mapSize : row >= 0;
                             row += direction == MoveDirection.Up ? 1 : -1)
                        {
                            if (NumbersControllers[row, originalColumn])
                            {
                                if (NumbersControllers[row, originalColumn].Number == NumbersControllers[originalRow, originalColumn].Number)
                                {
                                    foundNumber = NumbersControllers[row, originalColumn];
                                    return true;
                                }
                                return false;
                            }
                        }
                        return false;
                    case MoveDirection.Right:
                    case MoveDirection.Left:
                        for (int column = originalColumn + (direction == MoveDirection.Right ? 1 : -1);
                            direction == MoveDirection.Right ? column < mapSize : column >= 0;
                            column += direction == MoveDirection.Right ? 1 : -1)
                        {
                            if (NumbersControllers[originalRow, column])
                            {
                                if (NumbersControllers[originalRow, column].Number == NumbersControllers[originalRow, originalColumn].Number)
                                {
                                    foundNumber = NumbersControllers[originalRow, column];
                                    return true;
                                }
                                return false;
                            }
                        }
                        break;
                }
                return false;
            }
        }

        private bool IsGameOver()
        {
            foreach (var number in NumbersControllers)
            {
                if (number == null)
                    return false;
            }

            for (int row = 0; row < mapSize; row++)
            {
                for (int column = 0; column < mapSize; column++)
                {
                    if (column > 0 && NumbersControllers[row, column - 1].Number == NumbersControllers[row, column].Number)
                        return false;
                    if (row > 0 && NumbersControllers[row - 1, column].Number == NumbersControllers[row, column].Number)
                        return false;
                }
            }

            return true;
        }

        public void OnControlTypeChanged()
        {
            gameplayControl.Cancel(() => gameplayControl = Settings.UseAccelerometer ? (GameplayControl)new AccelerometerControl() : new TouchControl());
        }

        private void Update()
        {
            gameplayControl.Update();

            if (Input.GetMouseButtonDown(0) && CanTapToPlayAgain)
            {
                SceneManager.LoadScene(GAME_SCENE_INDEX);
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(MAIN_MENU_SCENE_INDEX);
            }

#if UNITY_EDITOR
            Testing.Input.Update();

            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("R");
                CreateNewNumberAndCheckGameOver();
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                Debug.Log("G");
                GameOver = true;
                WorldTypeDependency.OnGameOver(() => gameOverInfo.gameObject.SetActive(true));
            }
#endif
        }

        private void OnDestroy() => Instance = null;
    }
}