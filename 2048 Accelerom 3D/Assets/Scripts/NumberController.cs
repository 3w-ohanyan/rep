using System;
using System.Collections;
using UnityEngine;
using TMPro;

namespace _2048Accelerom3D
{
    public class NumberController : MonoBehaviour
    {
        private TextMeshPro textMeshPro;
        public bool CannotUse { get; set; }

        private static int movingNumbersCount;
        public static int MovingNumbersCount
        {
            get => movingNumbersCount;
            private set
            {
                movingNumbersCount = value;
                if (value == 0)
                {
                    foreach (var numberController in GameController.Instance.NumbersControllers)
                    {
                        if (numberController)
                            numberController.CannotUse = false;
                    }
                }
            }
        }

        public int Number
        {
            get => int.Parse(textMeshPro.text);
            set
            {
                textMeshPro.text = value.ToString();
                textMeshPro.fontSize = ConstantsAndMore.NUMBERS_FONT_SIZES[textMeshPro.text.Length - 1];
            }
        }

        private bool moving;

        private void Awake() => textMeshPro = GetComponentInChildren<TextMeshPro>();

        public void Move(Vector3 moveTo)
        {
            StartCoroutine(InnerMove());
            IEnumerator InnerMove()
            {
                MovingNumbersCount++;
                moving = true;

                while (Vector3.Distance(transform.position, moveTo) > .025F)
                {
                    yield return null;
                    transform.position = Vector3.Lerp(transform.position, moveTo, Time.deltaTime * ConstantsAndMore.NUMBERS_MOVE_SPEED / Vector3.Distance(transform.position, moveTo));
                }
                transform.position = moveTo;

                moving = false;
                MovingNumbersCount--;
            }
        }

        public void WaitForDestruction(NumberController destructor, Vector3 destructorTargetCoordinate, Action callBack)
        {
            StartCoroutine(Inner());
            IEnumerator Inner()
            {
                while (!Mathf.Approximately(Vector3.Distance(destructor.transform.position, destructorTargetCoordinate), 0))
                {
                    yield return null;
                }

                callBack();

                GameController.Instance.WorldTypeDependency.PlayNumberDestroyEffect(this);

                foreach (var rb in gameObject.GetComponentsInChildren<Rigidbody>())
                {
                    Destroy(rb.gameObject, GameController.Instance.WorldTypeDependency is FireWorld ? ConstantsAndMore.FIRE_WORLD_NUMBER_DESTROY_DELAY : ConstantsAndMore.WATER_WORLD_NUMBER_DESTROY_DELAY);
                }

                Destroy(gameObject, GameController.Instance.WorldTypeDependency is FireWorld ? ConstantsAndMore.FIRE_WORLD_NUMBER_DESTROY_DELAY_FIXED : ConstantsAndMore.WATER_WORLD_NUMBER_DESTROY_DELAY_FIXED);
            }
        }

        private void OnDestroy()
        {
            if (moving)
                MovingNumbersCount--;
        }
    }
}