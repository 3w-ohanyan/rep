﻿using UnityEngine;

namespace _2048Accelerom3D
{
    static class ConstantsAndMore
    {
        public const int START_TIME_NUMBERS_COUNT = 2;
        public const int NEW_NUMBER_IS_TWO_POSSIBILITY = 10;
        public const float NUMBERS_MOVE_SPEED = 8F;
        public const float TOUCH_CONTROL_MIN_VALUE = .05F;
        public const float TOUCH_CONTROL_MAX_VALUE = .6F;

        public const float TOUCH_CONTROL_CAMERA_ROTATE_SPEED = 9F / TOUCH_CONTROL_MAX_VALUE;
        public const float TOUCH_CONTROL_CAMERA_MOVE_SPEED = 1F / TOUCH_CONTROL_MAX_VALUE;

        public static int TOUCH_CONTROL_SCREEN_DOWN_BORDER = (int)(Screen.height * .05F);
        public static int TOUCH_CONTROL_SCREEN_UP_BORDER = Screen.height - TOUCH_CONTROL_SCREEN_DOWN_BORDER;
        public static int TOUCH_CONTROL_SCREEN_LEFT_BORDER = (int)(Screen.width * .03F);
        public static int TOUCH_CONTROL_SCREEN_RIGHT_BORDER = Screen.width - TOUCH_CONTROL_SCREEN_LEFT_BORDER;

        public const float ACCELEROMETER_CONTROL_MAX_VALUE = .35F;

        public const float ACCELEROMETER_CONTROL_CAMERA_ROTATE_MULTIPLIER = 5F * 2F;
        public const float ACCELEROMETER_CONTROL_CAMERA_MOVE_MULTIPLIER = .8F * 2F;

        public const float ACCELEROMETER_CONTROL_CAMERA_ROTATE_SPEED = 20F;
        public const float ACCELEROMETER_CONTROL_CAMERA_MOVE_SPEED = 8F;

        public const float CAMERA_ROTATE_BACK_SPEED = 4.8F;
        public const float CAMERA_MOVE_BACK_SPEED = 2F;

        public static float[] NUMBERS_FONT_SIZES = new float[] { 8F, 6.5F, 4.5F, 3.5F, 2.75F, 2.25F };

        public const string NUMBER_MAIN_EFFECT_TAG = "NumberMainEffect";
        public const string NUMBER_DESTROY_EFFECT_TAG = "NumberDestroyEffect";
        public const string GROUND_TAG = "Ground";
        public const string FADE_IMAGE_TAG = "FadeImage";


        public const int FIRE_WORLD_NUMBER_DESTROY_DELAY_FIXED = 15;
        public static int FIRE_WORLD_NUMBER_DESTROY_DELAY => Random.Range(11, FIRE_WORLD_NUMBER_DESTROY_DELAY_FIXED + 1);


        public const int WATER_WORLD_NUMBER_DESTROY_DELAY_FIXED = 4;
        public static int WATER_WORLD_NUMBER_DESTROY_DELAY => Random.Range(2, WATER_WORLD_NUMBER_DESTROY_DELAY_FIXED + 1);

        public const float WATER_WORLD_GAME_OVER_MAX_FROST = .9F;
        public const float WATER_WORLD_GAME_OVER_FROST_ANIMATION_SPEED = .5F;

        public const int MAIN_MENU_SCENE_INDEX = 0;
        public const int GAME_SCENE_INDEX = 1;

        public const string DIRECTIONAL_LIGHT_TAG = "DirectionalLight";

        public const float MENU_SUN_ANIMAITON_SPEED = .5F;
        public const float MENU_SUN_DISAPPEAR_SPEED = 2F;
        public const float MENU_SUN_MIN_INTENSITY = .75F;
        public const float MENU_SUN_MAX_INTENSITY = 2F;
    }
}