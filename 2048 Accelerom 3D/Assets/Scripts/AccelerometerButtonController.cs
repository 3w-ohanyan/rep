using System;
using UnityEngine;
using UnityEngine.UI;

namespace _2048Accelerom3D
{

    [RequireComponent(typeof(Image), typeof(Collider))]
    public class AccelerometerButtonController : MonoBehaviour
    {
        private Image image;

        private void Start()
        {
            image = GetComponent<Image>();
            UpdateButtonStyle();
        }

        private void OnMouseUp()
        {
            Settings.UseAccelerometer = !Settings.UseAccelerometer;
            PlayerPrefs.SetInt(nameof(Settings.UseAccelerometer), Convert.ToInt32(Settings.UseAccelerometer));
            PlayerPrefs.Save();
            UpdateButtonStyle();
        }

        private void UpdateButtonStyle()
        {
            Color color = image.color;
            color.a = Settings.UseAccelerometer ? 1F : .25F;
            image.color = color;
        }
    }
}
