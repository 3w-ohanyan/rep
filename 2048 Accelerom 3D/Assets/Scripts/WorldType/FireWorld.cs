﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;

namespace _2048Accelerom3D
{
    public class FireWorld : IWorldType
    {
        public NumberController NumberPrefab => GameController.Instance.FireNumberPrefab;
        public Transform GroundPrefab => GameController.Instance.FireGroundPrefab;
        public Transform WallPrefab => GameController.Instance.FireWallPrefab;

        public void PlayNumberDestroyEffect(NumberController numberController)
        {
            numberController.GetComponentInChildren<TextMeshPro>().enabled = false;

            foreach (var rb in numberController.GetComponentsInChildren<Rigidbody>())
            {
                rb.isKinematic = false;
                rb.AddForce(new Vector3(UnityEngine.Random.Range(250F, 500F), UnityEngine.Random.Range(250F, 500F), UnityEngine.Random.Range(250F, 500F)), ForceMode.Impulse);
            }

            foreach (var ps in numberController.GetComponentsInChildren<ParticleSystem>())
            {
                if (ps.CompareTag(ConstantsAndMore.NUMBER_MAIN_EFFECT_TAG))
                {
                    ps.Stop();
                }

                else if (ps.CompareTag(ConstantsAndMore.NUMBER_DESTROY_EFFECT_TAG))
                    ps.Play();
            }
        }

        public void OnStart() => RenderSettings.skybox = GameController.Instance.FireWorldSkyboxMaterial;

        public void OnGameOver(Action callback)
        {
            Vector3 effectsPosition = GameObject.FindGameObjectWithTag(ConstantsAndMore.GROUND_TAG).transform.position + new Vector3(0F, 1.5F, 0F);

            ParticleSystem explosion = GameObject.Instantiate(GameController.Instance.FireWorldGameOverExplosionPrefab);
            explosion.transform.position = effectsPosition;
            explosion.Play();

            ParticleSystem fire = GameObject.Instantiate(GameController.Instance.FireWorldGameOverFirePrefab);
            fire.transform.position = effectsPosition;

            GameController.Instance.StartCoroutine(Look());
            IEnumerator Look()
            {
                while (true)
                {
                    fire.transform.LookAt(Camera.main.transform);
                    yield return null;
                }
            }
            fire.Play();

            callback();
        }
    }
}
