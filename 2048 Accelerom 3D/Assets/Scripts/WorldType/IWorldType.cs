﻿using System;
using UnityEngine;

namespace _2048Accelerom3D
{
    public interface IWorldType
    {
        void OnStart();
        void OnGameOver(Action callback);
        void PlayNumberDestroyEffect(NumberController numberController);
        NumberController NumberPrefab { get; }
        Transform GroundPrefab { get; }
        Transform WallPrefab { get; }
    }
}
