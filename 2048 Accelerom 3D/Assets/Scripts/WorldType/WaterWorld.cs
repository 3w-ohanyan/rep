﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;

namespace _2048Accelerom3D
{
    public class WaterWorld : IWorldType
    {
        public NumberController NumberPrefab => GameController.Instance.WaterNumberPrefab;
        public Transform GroundPrefab => GameController.Instance.WaterGroundPrefab;
        public Transform WallPrefab => GameController.Instance.WaterWallPrefab;

        public void PlayNumberDestroyEffect(NumberController numberController)
        {
            numberController.GetComponentInChildren<TextMeshPro>().enabled = false;

            foreach (var rb in numberController.GetComponentsInChildren<Rigidbody>())
            {
                rb.isKinematic = false;
                rb.AddForce(new Vector3(UnityEngine.Random.Range(100F, 200F), 0F, UnityEngine.Random.Range(100F, 200F)), ForceMode.Impulse);
            }

            ParticleSystem ps = numberController.GetComponentInChildren<ParticleSystem>();
            if (ps.CompareTag(ConstantsAndMore.NUMBER_DESTROY_EFFECT_TAG))
                ps.Play();
        }

        public void OnStart()
        {
            Camera.main.GetComponent<FrostEffect>().enabled = true;
            GameObject.FindGameObjectWithTag(ConstantsAndMore.DIRECTIONAL_LIGHT_TAG).GetComponent<Light>().enabled = true;
            GameObject.Instantiate(GameController.Instance.WaterWorldAdditionsPrefab);
        }

        public void OnGameOver(Action callback)
        {
            FrostEffect frost = Camera.main.GetComponent<FrostEffect>();
            GameController.Instance.StartCoroutine(Animation());
            IEnumerator Animation()
            {
                while (!Mathf.Approximately(frost.maxFrost, ConstantsAndMore.WATER_WORLD_GAME_OVER_MAX_FROST))
                {
                    frost.maxFrost = Mathf.MoveTowards(frost.maxFrost, ConstantsAndMore.WATER_WORLD_GAME_OVER_MAX_FROST, Time.deltaTime * ConstantsAndMore.WATER_WORLD_GAME_OVER_FROST_ANIMATION_SPEED);
                    yield return null;
                }

                callback();
            }
        }
    }
}