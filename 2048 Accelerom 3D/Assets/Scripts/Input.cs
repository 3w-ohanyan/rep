﻿using UnityEngine;

namespace _2048Accelerom3D.Testing
{
    public class Input
    {
        public static Vector3 acceleration;

        public static void Update(KeyCode upKey = KeyCode.Keypad8, KeyCode downKey = KeyCode.Keypad2, KeyCode rightKey = KeyCode.Keypad6, KeyCode leftKey = KeyCode.Keypad4, float accelerationSpeed = 1F)
        {
            if (UnityEngine.Input.GetKey(upKey))
                acceleration.y = Mathf.Clamp(acceleration.y + 0.01F * accelerationSpeed, -1F, 1F);
            else if (UnityEngine.Input.GetKey(downKey))
                acceleration.y = Mathf.Clamp(acceleration.y - 0.01F * accelerationSpeed, -1F, 1F);
            else if (UnityEngine.Input.GetKey(rightKey))
                acceleration.x = Mathf.Clamp(acceleration.x + 0.01F * accelerationSpeed, -1F, 1F);
            else if (UnityEngine.Input.GetKey(leftKey))
                acceleration.x = Mathf.Clamp(acceleration.x - 0.01F * accelerationSpeed, -1F, 1F);
        }
    }
}
