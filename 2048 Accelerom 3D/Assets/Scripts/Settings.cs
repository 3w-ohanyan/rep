﻿namespace _2048Accelerom3D
{
    static class Settings
    {
        private static bool useAccelerometer;
        public static bool UseAccelerometer
        {
            get => useAccelerometer;
            set
            {
                useAccelerometer = value;
                if(GameController.Instance)
                    GameController.Instance.OnControlTypeChanged();
            }
        }
        public static bool FireWorldSelected { get; set; }
    }
}
