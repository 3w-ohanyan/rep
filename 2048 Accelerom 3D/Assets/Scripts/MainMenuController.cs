using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace _2048Accelerom3D
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField]
        private Image waterWorldButtonImage;

        [SerializeField]
        private Image fireWorldButtonImage;

        [SerializeField]
        private Light sun;

        private bool worldStarting;

        private void Start()
        {
            Settings.UseAccelerometer = Convert.ToBoolean(PlayerPrefs.GetInt(nameof(Settings.UseAccelerometer), 0));
            waterWorldButtonImage.alphaHitTestMinimumThreshold = 1F;
            fireWorldButtonImage.alphaHitTestMinimumThreshold = 1F;
            StartCoroutine(StartSunAnimation());
        }

        private IEnumerator StartSunAnimation()
        {
            float targetIntensity;

            while (true)
            {
                if (!worldStarting)
                {
                    targetIntensity = UnityEngine.Random.Range(ConstantsAndMore.MENU_SUN_MIN_INTENSITY, ConstantsAndMore.MENU_SUN_MAX_INTENSITY);
                    yield return StartCoroutine(GoToTargetIntensity());
                }
                else
                {
                    StartCoroutine(GoToZeroIntensity());
                    yield break;
                }
            }

            IEnumerator GoToTargetIntensity()
            {
                while (!Mathf.Approximately(sun.intensity, targetIntensity) && !worldStarting)
                {
                    sun.intensity = Mathf.MoveTowards(sun.intensity, targetIntensity, Time.deltaTime * ConstantsAndMore.MENU_SUN_ANIMAITON_SPEED);
                    yield return null;
                }
            }

            IEnumerator GoToZeroIntensity()
            {
                while (!Mathf.Approximately(sun.intensity, 0F))
                {
                    sun.intensity = Mathf.MoveTowards(sun.intensity, 0F, Time.deltaTime * ConstantsAndMore.MENU_SUN_DISAPPEAR_SPEED);
                    yield return null;
                }
            }
        }

        public void StartWorld(bool fireWorld)
        {
            if (!worldStarting)
            {
                worldStarting = true;
                Settings.FireWorldSelected = fireWorld;
                GameObject.FindGameObjectWithTag(ConstantsAndMore.FADE_IMAGE_TAG).GetComponent<Animation>().Play("BlackFadeOut");
                Invoke(nameof(FadeOutEnded), 2F);
            }
        }

        private void FadeOutEnded()
        {
            SceneManager.LoadScene(ConstantsAndMore.GAME_SCENE_INDEX);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }
}