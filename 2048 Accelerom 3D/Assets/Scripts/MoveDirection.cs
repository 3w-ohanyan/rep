﻿namespace _2048Accelerom3D
{
    public enum MoveDirection
    {
        None,
        Up,
        Right,
        Down,
        Left
    }
}