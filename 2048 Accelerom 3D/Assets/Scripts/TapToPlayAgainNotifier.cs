using UnityEngine;

namespace _2048Accelerom3D 
{
    [RequireComponent(typeof(Animation))]
    public class TapToPlayAgainNotifier : MonoBehaviour
    {
        public void Notify()
        {
            GameController.Instance.CanTapToPlayAgain = true;
        }
    }
}